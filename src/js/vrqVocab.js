function getUrlVars() {
    var vars = [],
        hash;
    var hashes = window.location.href
        .slice(window.location.href.indexOf("?") + 1)
        .split("&");
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split("=");
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function hideLoader() {
    $(".showbox").hide();
}

function showError(err) {
    $("body").empty();
    $("body").append($("<strong>" + err + "</strong>"));
}

function randomInt(ceil) {
    return Math.floor(Math.random() * Math.floor(ceil));
}

const WORD_BOX_SIZE = 20;
const QUIZ_SIZE = 5;

function getWords() {
    let data = window.csv;

    let indicies = [];

    while (indicies.length < WORD_BOX_SIZE) {
        let rnd = randomInt(data.length);
        while (indicies.indexOf(rnd) != -1) {
            rnd = randomInt(data.length);
        }

        indicies.push(rnd);
    }

    let words = [];
    for (const i of indicies) {
        words.push({
            term: data[i][0],
            def: data[i][1]
        });
    }

    return words;
}

const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

function fillWordBank(words) {
    for (let i = 0; i < WORD_BOX_SIZE; i++) {
        let elem = $("<h4></h4>");
        $(elem).addClass("bank-entry");
        $(elem).text(letters[i] + ". " + words[i].term);
        $("#word-bank-scrollable").append(elem);
    }
}

function fillQuiz(words) {
    for (let i = 0; i < words.length; i++) {
        words[i].letter = letters[i];
    }
    let indicies = [];
    while (indicies.length < QUIZ_SIZE) {
        let rnd = randomInt(WORD_BOX_SIZE);
        while (indicies.indexOf(rnd) != -1) {
            rnd = randomInt(WORD_BOX_SIZE);
        }

        indicies.push(rnd);
    }

    window.quizAnswers = "";

    for (let i = 0; i < QUIZ_SIZE; i++) {
        let elem = $("<div></div>");
        elem.addClass("quiz-question");
        let def = $("<h4></h4>");
        def.text((i + 1).toString() + ". " + words[indicies[i]].def);
        elem.append(def);
        let input = $("<input></input>");
        input.addClass("form-control");
        input.addClass("quiz-input");
        input.attr("maxlength", 1);
        input.attr("data-index", i);
        elem.append(input);
        $("#quiz-scrollable").append(elem);
        let buttons = $("#quiz-button-box");
        $("#quiz-scrollable").append(buttons);
        window.quizAnswers += words[indicies[i]].letter;
    }
}

function setupQuiz() {
    const words = getWords();

    fillWordBank(words);
    fillQuiz(words);
}

function splitQuoted(str) {
    let res = [];
    let quoteProtected = false;
    let buffer = "";
    for (let i = 0; i < str.length; i++) {
        const char = str.charAt(i);
        if (char == '"') {
            quoteProtected = !quoteProtected;
        } else if (char == "," && !quoteProtected) {
            res.push(buffer);
            buffer = "";
        } else {
            buffer += char;
        }
    }
    res.push(buffer);
    return res;
}

function quizCheck() {
    $(".feedback-text").remove();
    for (let i = 0; i < QUIZ_SIZE; i++) {
        let input = $("[data-index='" + i.toString() + "']")[0];
        let tx = $("<h5></h5>");
        tx.addClass("feedback-text");
        if (input.value.toUpperCase() == window.quizAnswers.charAt(i)) {
            tx.text("Correct!");
            tx.css("color", "green");
        } else {
            tx.text(
                "Incorrect, " + window.quizAnswers.charAt(i) + " was the answer"
            );
            tx.css("color", "red");
        }
        $(input)
            .parent()
            .append(tx);
    }
}

function resetQuiz() {
    $(".bank-entry").remove();
    $(".quiz-question").remove();
    $(".feedback-text").remove();
    setupQuiz();
}
$(document).ready(() => {
    $("#quiz-done").on("click", function() {
        quizCheck();
    });
    $("#quiz-reset").on("click", function() {
        resetQuiz();
    });
    setTimeout(() => {
        const qs = getUrlVars();
        const ch = qs["ch"];
        if (ch == undefined) {
            hideLoader();
            showError("NO CH SPECIFIER");
            return;
        }

        const config = {
            apiKey: "AIzaSyCBk40o1scCRoGg46royuTmm8VtMzx4LEM",
            projectId: "eurostudy-531fa",
            storageBucket: "eurostudy-531fa.appspot.com"
        };

        firebase.initializeApp(config);

        const storage = firebase.storage();

        let dataRef = storage.ref("vrqVocab/sets/" + ch + ".csv");

        dataRef
            .getDownloadURL()
            .then(url => {
                fetch(url)
                    .then(resp => {
                        return resp.text();
                    })
                    .then(text => {
                        try {
                            window.csv = text.split("\n");
                            for (let i = 0; i < window.csv.length; i++) {
                                window.csv[i] = splitQuoted(window.csv[i]);
                            }

                            window.csv.shift();

                            setupQuiz();
                            hideLoader();
                            $("#root").css("display", "grid");
                        } catch (err) {
                            console.log(err);
                        }
                    })
                    .catch(err => {
                        hideLoader();
                        showError("FAILED TO FETCH RESOURCE");
                    });
            })
            .catch(err => {
                hideLoader();
                switch (err.code()) {
                    case "storage/object-not-found":
                        showError("NO OBJECT FOUND");
                        break;
                    case "storage/unauthorized":
                        showError("REQUEST NOT AUTHORIZED");
                        break;
                    case "storage/unkown":
                        showError("UNKNOWN ERROR");
                        break;
                }

                return;
            });
    }, 0);
});
