const { src, dest, series, parallel, task, watch } = require("gulp");
const pug = require("gulp-pug");
const babel = require("gulp-babel");
const uglify = require("gulp-uglify");
const rimraf = require("rimraf");
const pump = require("pump");
const sass = require("gulp-sass");
const cleanCss = require("gulp-clean-css");

task("buildJs", function(cb) {
    pump(
        [
            src("./src/js/*.js"),
            babel({
                presets: ["@babel/env"]
            }),
            uglify(),
            dest("./dist/static/js")
        ],
        cb
    );
});

task("buildPug", function(cb) {
    pump([src("./src/pug/*.pug"), pug(), dest("./dist")], cb);
});

task("buildSass", function(cb) {
    pump(
        [src("src/sass/*.sass"), sass(), cleanCss(), dest("dist/static/css")],
        cb
    );
});

task("copyPublic", function(cb) {
    pump([src("public/**"), dest("dist/static/")], cb);
});

task("clean", function(cb) {
    rimraf("dist", cb);
});

task(
    "build",
    series("clean", parallel("copyPublic", "buildPug", "buildJs", "buildSass"))
);

task("cleanJs", cb => rimraf("dist/static/js", cb));
task("cleanCss", cb => rimraf("dist/static/css", cb));
task("cleanHtml", cb => rimraf("dist/*.html", cb));
task("cleanStatic", cb => rimraf("dist/static", cb));

task("watch", () => {
    watch("src/js/**", series("cleanJs", "buildJs"));
    watch("src/sass/**", series("cleanCss", "buildSass"));
    watch("src/pug/**", series("cleanHtml", "buildPug"));
    watch(
        "public/**",
        series("cleanStatic", parallel("buildSass", "buildJs", "copyPublic"))
    );
});
